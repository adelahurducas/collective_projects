import urllib2 as url
from bs4 import BeautifulSoup

def printMultiLine(text):
    words=text.split()
    lines=len(words)//15
    i=0
    count=0
    for i in xrange(0,lines):
        line=""
        #print words[count:count+15]
        for j in  xrange(count,count+15):
            line+=words[j]+' '
        count+=15
        print "{:<10}".format(line)
    line=""
    if len(words)%15!=0:
        for i in xrange(count,len(words)):
            line+=words[i]+" "
    print "{:<10}".format(line)
    print

def display_article(soup):
    contentFather=soup.find('div',attrs={ 'class' : 'contentFather'})
    content = contentFather.find('div', attrs={'class': 'content'})
    center_side=content.find('div', attrs={'class' : 'center_side'})
    center=center_side.find('div', attrs={'class': 'center'})
    articol_render=center.find('div', attrs={'class' : 'articol_render'})
    articleContent=articol_render.find('div' , attrs={'id' : 'articleContent'})
    printMultiLine(articleContent.text)

def open_DNA_articles():
    soup=BeautifulSoup(url.urlopen("http://www.hotnews.ro/"),'html5lib')
    all_divs=soup.find_all('div',class_='articol_lead_full')
    count=0
    i=0
    while count<=2 and i<len(all_divs):
        lead=all_divs[i].find('div', attrs={'class': 'lead'})
        if 'DNA' in lead.text:
            print lead.text
            title=all_divs[i].find('h2', attrs={'class' :  'article_title'})
            article_url=title.find('a')
            soup_article=BeautifulSoup(url.urlopen(article_url.get('href')),'html5lib')
            display_article(soup_article)
            count+=2
        i+=1

open_DNA_articles()

