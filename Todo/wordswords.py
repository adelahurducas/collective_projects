def splits(word):
    """
    returns a list of all posible splits of word
    include the empty splits
    returns a list of tuples.
    [('', 'ana'), ('a', 'na'), ('an', 'a'), ('ana', '')]
    """
    rez=[]
    for i in xrange(0,len(word)+1):
        t=word[0:i],word[i:]
        rez.append(t)
    return rez


print splits('banaana')

def deletes(word):
    """
    all misspellings of word caused by omitting a letter
    use the splits function
    #>>> 'rockets' not in deletes('rocket')
    True
    #>>> 'roket' in deletes('rocket')
    True
    """
    result=[]
    for i in xrange(0,len(word)):
        misspell=word[0:i]+word[i+1:]
        result.append(misspell)
    return result

print deletes('rocket')