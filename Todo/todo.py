#!/usr/bin/python
import os
import json
import textwrap
import argparse
import datetime
import sys
import prettydate

class Todo(object):
    def __init__(self, message, id_=None, done=False, created=None):
        self.message = message
        self.id = id_
        self.done = done
        self.created = created

    def when(self, date):
        current_date = datetime.datetime.now()
        delta_seconds = datetime.timedelta(current_date, date)/ 1000000
        formats = ['sec', 'min', 'hours', 'days', 'weeks']
        i = 0
        for t in formats:
            if i < 3:
                if delta_seconds / 60 < 60:
                    return str(delta_seconds) + " " + t + " ago"
                delta_seconds = delta_seconds / 60
            elif i == 3:
                if delta_seconds / 24 < 24:
                    return str(delta_seconds) + " " + t + " ago"
                delta_seconds = delta_seconds / 24
            else:
                return str(delta_seconds) + " " + t + " ago"

    def printTodo(self):
        print "id "+colored_text(str(self.id),GREEN) + " todo " +colored_text( prettydate.pretty_date(self.created), LBLUE)
        print "{:^40}".format(self.message)


class DB(object):
    def __init__(self):
        self.todo_list = []
        self.load()

    def load(self):
        f = None
        try:
            f = open("/home/adela/module1-exercises/day1/shell_exercises/todos.txt")
            for line in f:
                self.todo_list.append(self._todo_from_json(line))
        finally:
            if f is not None:
                f.close()

    def _ensure_id(self):
        return len(self.todo_list) + 1

    def _todo_to_json(self, obj):
        jsonTodo = {}
        jsonTodo["message"] = obj.message
        jsonTodo["id"] = obj.id
        jsonTodo["done"] = str(obj.done)
        jsonTodo["created"] = obj.created.isoformat()
        return json.dumps(jsonTodo)

    def _todo_from_json(self, rt):
        todo = json.loads(rt)
        format="%Y-%m-%dT%H:%M:%S.%f"
        if todo["done"] == 'True':
            todoObj = Todo(todo['message'], todo['id'], True, datetime.datetime.strptime(todo['created'],format))
        else:
            todoObj = Todo(todo['message'], todo['id'], False, datetime.datetime.strptime(todo['created'],format))
        return todoObj

    def save(self):
        f = None
        try:
            f = open("/home/adela/module1-exercises/day1/shell_exercises/todos.txt", 'w')
            for todo in self.todo_list:
                f.write(self._todo_to_json(todo)+"\n")
        finally:
            if f is not None:
                f.close()

    def add(self, t):
        message = t
        id = self._ensure_id()
        created = datetime.datetime.now()
        t = Todo(message, id, False, created)
        self.todo_list.append(t)
        self.save()

    def get(self, id):
        for todo in self.todo_list:
            if todo.id==id:
                return todo


def human_time(dt): pass


def toesc(c):
    """
    Returns the ansii escape with the code c
    \x1b[cm
    """
    return '\x1b[' +str(c)+'m'
ENDC=toesc(0)
LBLACK, LRED, LGREEN, LYELLOW, LBLUE, LPURPLE, LCYAN, LWHITE=[toesc(i) for i in range (90,98)]
GREEN=toesc(32)
def colored_text(text, color=ENDC):
    """ returns the text surrounded by ansii escapes that would make it print in the given color on a terminal """
    return color+text+ENDC

def command_list():
    db=DB()
    for x in db.todo_list:
        if not x.done:
            x.printTodo()

def command_add(ns):
    db = DB()
    db.add(ns)
    print "Todo added"

def command_done(ns):
    db=DB()
    todo=db.get(ns)
    todo.done=True
    db.save()
    print"Todo setted to done"

def main():
    cmd=sys.argv[1]
    if cmd=='add':
        message=""
        for x in sys.argv[2:]:
            message+=x
        command_add(message)
    elif cmd=='done':
        command_done(int(sys.argv[2]))
    elif cmd=='list':
        command_list()
    else:
        print "Command "+cmd+" not found"

main()
